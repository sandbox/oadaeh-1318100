<?php
// $Id: comment-wrapper.tpl.php,v 1.3 2009/11/01 19:47:40 johnalbin Exp $

/**
 * @file
 * Default theme implementation to wrap comments.
 *
 * Available variables:
 * - $content: All comments for a given page. Also contains sorting controls
 *   and comment forms if the site is configured for it.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default value has the following:
 *   - comment-wrapper: The current template type, i.e., "theming hook".
 *
 * The following variables are provided for contextual information.
 * - $node: Node object the comments are attached to.
 * The constants below the variables show the possible values and should be
 * used for comparison.
 * - $display_mode
 *   - COMMENT_MODE_FLAT_COLLAPSED
 *   - COMMENT_MODE_FLAT_EXPANDED
 *   - COMMENT_MODE_THREADED_COLLAPSED
 *   - COMMENT_MODE_THREADED_EXPANDED
 * - $display_order
 *   - COMMENT_ORDER_NEWEST_FIRST
 *   - COMMENT_ORDER_OLDEST_FIRST
 * - $comment_controls_state
 *   - COMMENT_CONTROLS_ABOVE
 *   - COMMENT_CONTROLS_BELOW
 *   - COMMENT_CONTROLS_ABOVE_BELOW
 *   - COMMENT_CONTROLS_HIDDEN
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * @see template_preprocess_comment_wrapper()
 */
?>
<div id="comments" class="<?php print $classes; ?>">
  <?php if ($node->type != 'forum'): ?>
    <?php if ($plastictheme_enable_rounded_corners): ?>
      <?php if ($plastictheme_enable_upper_left): ?>
        <?php if ($plastictheme_enable_upper_right): ?>
          <span class="roundcorner-comment-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
        <?php else: ?>
          <span class="roundcorner-comment-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
        <?php endif; ?>
      <?php elseif ($plastictheme_enable_upper_right): ?>
        <span class="roundcorner-comment-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
      <?php endif; ?>
    <?php endif; ?>

    <h2 class="title"><?php print t('Comments'); ?></h2>
  <?php endif; ?>

  <?php print $content; ?>
</div>
