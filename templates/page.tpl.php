<?php
// $Id: page.tpl.php,v 1.26.2.3 2010/06/26 15:36:04 johnalbin Exp $

/**
 * @file
 * Theme implementation to display a single Drupal page.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $css: An array of CSS files for the current page.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/garland.
 * - $is_front: TRUE if the current page is the front page. Used to toggle the mission statement.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Page metadata:
 * - $language: (object) The language the site is being displayed in.
 *   $language->language contains its textual representation.
 *   $language->dir contains the language direction. It will either be 'ltr' or 'rtl'.
 * - $head_title: A modified version of the page title, for use in the TITLE tag.
 * - $head: Markup for the HEAD section (including meta tags, keyword tags, and
 *   so on).
 * - $styles: Style tags necessary to import all CSS files for the page.
 * - $scripts: Script tags necessary to load the JavaScript files and settings
 *   for the page.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It should be placed within the <body> tag. When selecting through CSS
 *   it's recommended that you use the body tag, e.g., "body.front". It can be
 *   manipulated through the variable $classes_array from preprocess functions.
 *   The default values can be one or more of the following:
 *   - front: Page is the home page.
 *   - not-front: Page is not the home page.
 *   - logged-in: The current viewer is logged in.
 *   - not-logged-in: The current viewer is not logged in.
 *   - node-type-[node type]: When viewing a single node, the type of that node.
 *     For example, if the node is a "Blog entry" it would result in "node-type-blog".
 *     Note that the machine name will often be in a short form of the human readable label.
 *   - page-views: Page content is generated from Views. Note: a Views block
 *     will not cause this class to appear.
 *   - page-panels: Page content is generated from Panels. Note: a Panels block
 *     will not cause this class to appear.
 *   The following only apply with the default 'sidebar_first' and 'sidebar_second' block regions:
 *     - two-sidebars: When both sidebars have content.
 *     - no-sidebars: When no sidebar content exists.
 *     - one-sidebar and sidebar-first or sidebar-second: A combination of the
 *       two classes when only one of the two sidebars have content.
 * - $node: Full node object. Contains data that may not be safe. This is only
 *   available if the current page is on the node's primary url.
 * - $menu_item: (array) A page's menu item. This is only available if the
 *   current page is in the menu.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $mission: The text of the site mission, empty when display has been disabled
 *   in theme settings.
 *
 * Navigation:
 * - $search_box: HTML to display the search box, empty if search has been disabled.
 * - $primary_links (array): An array containing the Primary menu links for the
 *   site, if they have been configured.
 * - $secondary_links (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title: The page title, for use in the actual HTML content.
 * - $messages: HTML for status and error messages. Should be displayed prominently.
 * - $tabs: Tabs linking to any sub-pages beneath the current page (e.g., the
 *   view and edit tabs when displaying a node).
 * - $help: Dynamic help text, mostly for admin pages.
 * - $content: The main content of the current page.
 * - $feed_icons: A string of all feed icons for the current page.
 *
 * Footer/closing data:
 * - $footer_message: The footer message as defined in the admin settings.
 * - $closure: Final closing markup from any modules that have altered the page.
 *   This variable should always be output last, after all other dynamic content.
 *
 * Helper variables:
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 *
 * Regions:
 * - $content_top: Items to appear above the main content of the current page.
 * - $content_bottom: Items to appear below the main content of the current page.
 * - $navigation: Items for the navigation bar.
 * - $sidebar_first: Items for the first sidebar.
 * - $sidebar_second: Items for the second sidebar.
 * - $header: Items for the header region.
 * - $footer: Items for the footer region.
 * - $page_closure: Items to appear below the footer.
 *
 * The following variables are deprecated and will be removed in Drupal 7:
 * - $body_classes: This variable has been renamed $classes in Drupal 7.
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see zen_preprocess()
 * @see zen_process()
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">

<head>
  <title><?php print $head_title; ?></title>

  <?php
    print $head;
    print $styles;
    print $scripts;
    print $plastictheme_header_height;
    print $plastictheme_page_opener;

    // If a font family other than that specified in the .css files was
    // selected to use, add that.
    if ($plastictheme_font_family) {
      print $plastictheme_font_family;
    }

    // If any custom colors were specified in the theme settings UI, add them.
    if ($plastictheme_custom_colors) {
      print $plastictheme_custom_colors;

      // We have to apply exceptions to the rules.
      if ($plastictheme_content_background) {
        // Removes the round corner bottom on comment reply page and the extra
        // round corner bottom on comment preview page.
        $content_pages = array('node', 'blog', 'comment', 'articles');

        if (arg(0) == 'comment' && arg(1) == 'reply') {
          // Set the background to the color specified for comments.
          print $plastictheme_comment_content_background;
        }
        elseif (arg(0) == 'node' && arg(2)) {
          // Set the background to the color specified for nodes.
          print $plastictheme_node_content_background;
        }
        elseif (!arg(0) || (arg(0) == 'node' && (arg(1) == 'add' && !arg(2))) || (!in_array(arg(0), $content_pages))) {
          // Set the background to the color specified for other content.
          print $plastictheme_nonnode_content_background;
        }
        elseif (arg(0) == 'node' && arg(2) == 'edit' && $title == 'Preview') {
//        elseif ($title == 'Preview') {
          // Set the background to the color to the deafult page background
          // color for preview pages.
          print $plastictheme_nonnode_content_background;
        }
      }
    }

/*
    // Determine whether to add a spacer around the page's content, depending
    // on what the content is.
    if ($is_front || arg(0) == 'comment' || arg(0) == 'blog' || arg(0) == 'taxonomy' || $title == 'Preview') {
      // Do not add it for the front page, the comment list page, the blog
      // list page, and the preview pages.
      print "<style type=\"text/css\">\n#content-area { padding: 1px 0 0 0; }\n</style>";
    }
    else {
      // Add it for every other page.
      print "<style type=\"text/css\">\n#content-area { padding-top: 1px; padding-right: 0.2em; padding-left: 0.2em; }\n</style>";
    }
*/
  ?>
</head>

<body class="<?php print $classes; ?>">
  <?php if ($primary_links): ?>
    <div id="skip-link"><a href="#main-menu"><?php print t('Jump to Navigation'); ?></a></div>
  <?php endif; ?>

  <div id="page-wrapper"><div id="page">
    <div id="header"><div class="section clearfix">
      <?php if ($logo || $site_name || $site_slogan || $search_box): ?>
        <?php if ($plastictheme_enable_rounded_corners): ?>
          <?php if ($plastictheme_enable_upper_left): ?>
            <?php if ($plastictheme_enable_upper_right): ?>
              <span class="roundcorner-page-opener-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
            <?php else: ?>
              <span class="roundcorner-page-opener-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
            <?php endif; ?>
          <?php elseif ($plastictheme_enable_upper_right): ?>
            <span class="roundcorner-page-opener-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
          <?php endif; ?>
        <?php endif; ?>

        <div id="page-opener">
          <?php if ($logo): ?>
            <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" id="logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
          <?php endif; ?>

          <?php if ($site_name || $site_slogan): ?>
            <div id="name-and-slogan">
              <?php if ($site_name): ?>
                <?php if ($title): ?>
                  <div id="site-name"><strong>
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                  </strong></div>
                <?php else: /* Use h1 when the content title is empty */ ?>
                  <h1 id="site-name">
                    <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
                  </h1>
                <?php endif; ?>
              <?php endif; ?>

              <?php if ($site_slogan): ?>
                <div id="site-slogan"><?php print $site_slogan; ?></div>
              <?php endif; ?>
            </div> <!-- /#name-and-slogan -->
          <?php endif; ?>

          <?php if ($search_box): ?>
            <div id="search-box"><?php print $search_box; ?></div>
          <?php endif; ?>
        </div> <!-- /#page-opener -->

        <?php if ($plastictheme_enable_rounded_corners): ?>
          <?php if ($plastictheme_enable_lower_left): ?>
            <?php if ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-page-opener-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
            <?php else: ?>
              <span class="roundcorner-page-opener-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
            <?php endif; ?>
          <?php elseif ($plastictheme_enable_lower_right): ?>
            <span class="roundcorner-page-opener-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
          <?php endif; ?>
        <?php endif; ?>
      <?php endif; ?>

      <?php print $header; ?>
    </div></div> <!-- /.section, /#header -->

    <div id="main-wrapper"><div id="main" class="clearfix<?php if ($primary_links || $navigation) { print ' with-navigation'; } ?>">
      <div id="content" class="column"><div class="section">
        <?php if ($mission): ?>
          <?php if ($plastictheme_enable_rounded_corners): ?>
            <?php if ($plastictheme_enable_upper_left): ?>
              <?php if ($plastictheme_enable_upper_right): ?>
                <span class="roundcorner-mission-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
              <?php else: ?>
                <span class="roundcorner-mission-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_upper_right): ?>
              <span class="roundcorner-mission-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
            <?php endif; ?>
          <?php endif; ?>

          <div id="mission"><?php print $mission; ?></div>

          <?php if ($plastictheme_enable_rounded_corners): ?>
            <?php if ($plastictheme_enable_lower_left): ?>
              <?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-mission-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              <?php else: ?>
                <span class="roundcorner-mission-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-mission-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            <?php endif; ?>
          <?php endif; ?>
        <?php endif; ?>

        <?php print $highlight; ?>

        <?php print $breadcrumb; ?>

        <?php print $messages; ?>

        <?php if ($title): ?>
          <?php if ($plastictheme_enable_rounded_corners): ?>
            <?php if ($plastictheme_enable_upper_left): ?>
              <?php if ($plastictheme_enable_upper_right): ?>
                <span class="roundcorner-title-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
              <?php else: ?>
                <span class="roundcorner-title-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_upper_right): ?>
              <span class="roundcorner-title-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
            <?php endif; ?>
          <?php endif; ?>

          <h1 class="title"><?php print $title; ?></h1>

          <!-- Adds the rounded corner bottom to the title bar for the blog and taxonomy pages. -->
          <!--?php if ((arg(0) == 'blog' && arg(1) == '') || arg(0) == 'taxonomy'): ?-->
          <!-- Adds the rounded corner bottom to the title bar for the blog page. -->
          <?php if (arg(0) == 'blog' && arg(1) == ''): ?>
            <?php if ($plastictheme_enable_rounded_corners): ?>
              <?php if ($plastictheme_enable_lower_left): ?>
                <?php if ($plastictheme_enable_lower_right): ?>
                  <span class="roundcorner-title-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
                <?php else: ?>
                  <span class="roundcorner-title-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
                <?php endif; ?>
              <?php elseif ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-title-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
              <?php endif; ?>
            <?php endif; ?>
          <?php elseif (arg(0) == 'comment' && arg(1) == 'reply'): ?>
            <p></p>  <!-- Adds space beneath the title. -->
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($tabs): ?>
          <div class="tabs"><?php print $tabs; ?></div>
        <?php endif; ?>

        <?php print $help; ?>

        <!-- This theme does not support a content_top block. -->
        <!--?php print $content_top; ?-->

        <div id="content-area">

          <!--?php if ($is_front && !$title): ?>
            < ?php if ($plastictheme_enable_rounded_corners): ?>
              < ?php if ($plastictheme_enable_upper_left): ?>
                < ?php if ($plastictheme_enable_upper_right): ?>
                  <span class="roundcorner-title-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
                < ?php else: ?>
                  <span class="roundcorner-title-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
                < ?php endif; ?>
              < ?php elseif ($plastictheme_enable_upper_right): ?>
                <span class="roundcorner-title-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
              < ?php endif; ?>
            < ?php endif; ?>
          < ?php endif; ?-->

          <?php print $content; ?>

          <?php if ($is_front && !$logged_in): ?>
            <?php if ($plastictheme_enable_rounded_corners): ?>
              <?php if ($plastictheme_enable_upper_left): ?>
                <?php if ($plastictheme_enable_upper_right): ?>
                  <span class="roundcorner-welcome-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner3"></span><span class="roundcorner3"></span></span>
                <?php else: ?>
                  <span class="roundcorner-welcome-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
                <?php endif; ?>
              <?php elseif ($plastictheme_enable_upper_right): ?>
                <span class="roundcorner-welcome-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
              <?php endif; ?>
            <?php endif; ?>
          <?php endif; ?>
        </div>

        <?php if ($plastictheme_enable_rounded_corners && $plastictheme_content_background): ?>
          <?php if (!arg(0) ||
                   (arg(0) == 'node' && arg(1) == 'add') ||
                   (!in_array(arg(0), $content_pages))): ?>
            <?php if ($plastictheme_enable_lower_left): ?>
              <?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-content-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              <?php else: ?>
                <span class="roundcorner-content-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-content-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            <?php endif; ?>
          <?php elseif (arg(0) == 'node' && arg(2)): ?>
            <?php if ($plastictheme_enable_lower_left): ?>
              <?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-node-edit-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              <?php else: ?>
                <span class="roundcorner-node-edit-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-node-edit-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            <?php endif; ?>
          <?php elseif (arg(0) == 'comment' && arg(1) == 'reply' && $title == 'Reply to comment'): ?>
          <!--?php elseif (arg(0) == 'comment' && arg(1) == 'reply'): ?-->
            <?php if ($plastictheme_enable_lower_left): ?>
              <?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-comment-reply-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              <?php else: ?>
                <span class="roundcorner-comment-reply-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-comment-reply-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            <?php endif; ?>
          <?php endif; ?>
        <?php endif; ?>

        <?php print $content_bottom; ?>

        <?php if ($feed_icons): ?>
          <div class="feed-icons"><?php print $feed_icons; ?></div>
        <?php endif; ?>
      </div></div> <!-- /.section, /#content -->

      <?php if ($primary_links || $secondary_links || $navigation): ?>
        <div id="navigation"><div class="section clearfix">
          <?php if ($primary_links): ?>
            <?php if ($secondary_links): ?>
              <div class="menu-half"><div class="menu-half-one">
            <?php endif; ?>

            <?php if ($plastictheme_enable_rounded_corners): ?>
              <?php if ($plastictheme_enable_upper_left): ?>
                <?php if ($plastictheme_enable_upper_right): ?>
                  <span class="roundcorner-main-menu-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
                <?php else: ?>
                  <span class="roundcorner-main-menu-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
                <?php endif; ?>
              <?php elseif ($plastictheme_enable_upper_right): ?>
                <span class="roundcorner-main-menu-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
              <?php endif; ?>
            <?php endif; ?>
          <?php endif; ?>

          <?php print theme(array('links__system_main_menu', 'links'), $primary_links,
            array(
              'id' => 'main-menu',
              'class' => 'links clearfix',
            ),
            array(
              'text' => t('Main menu'),
              'level' => 'h2',
              'class' => 'element-invisible',
            ));
          ?>

          <?php if ($primary_links): ?>
            <?php if ($plastictheme_enable_rounded_corners): ?>
              <?php if ($plastictheme_enable_lower_left): ?>
                <?php if ($plastictheme_enable_lower_right): ?>
                  <span class="roundcorner-main-menu-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
                <?php else: ?>
                  <span class="roundcorner-main-menu-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
                <?php endif; ?>
              <?php elseif ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-main-menu-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
              <?php endif; ?>
            <?php endif; ?>

            <?php if ($secondary_links): ?>
              </div>
            <?php endif; ?>
          <?php endif; ?>

          <?php if ($secondary_links): ?>
            <?php if ($primary_links): ?>
              <div class="menu-half-two">
            <?php endif; ?>

            <?php if ($plastictheme_enable_rounded_corners): ?>
              <?php if ($plastictheme_enable_upper_left): ?>
                <?php if ($plastictheme_enable_upper_right): ?>
                  <span class="roundcorner-secondary-menu-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
                <?php else: ?>
                  <span class="roundcorner-secondary-menu-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
                <?php endif; ?>
              <?php elseif ($plastictheme_enable_upper_right): ?>
                <span class="roundcorner-secondary-menu-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
              <?php endif; ?>
            <?php endif; ?>

            <?php print theme(array('links__system_secondary_menu', 'links'), $secondary_links,
              array(
                'id' => 'secondary-menu',
                'class' => 'links clearfix',
              ),
              array(
                'text' => t('Secondary menu'),
                'level' => 'h2',
                'class' => 'element-invisible',
              ));
            ?>

            <?php if ($plastictheme_enable_rounded_corners): ?>
              <?php if ($plastictheme_enable_lower_left): ?>
                <?php if ($plastictheme_enable_lower_right): ?>
                  <span class="roundcorner-secondary-menu-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
                <?php else: ?>
                  <span class="roundcorner-secondary-menu-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
                <?php endif; ?>
              <?php elseif ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-secondary-menu-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
              <?php endif; ?>
            <?php endif; ?>

            <?php if ($primary_links): ?>
              </div></div>
            <?php endif; ?>
          <?php endif; ?>

          <?php print $navigation; ?>
        </div></div> <!-- /.section, /#navigation -->
      <?php endif; ?>

      <?php print $sidebar_first; ?>

      <?php print $sidebar_second; ?>
    </div></div> <!-- /#main, /#main-wrapper -->

    <?php if ($footer || $footer_message): ?>
      <div id="footer"><div class="section">
        <?php if ($footer_message): ?>
          <?php if ($plastictheme_enable_rounded_corners): ?>
            <?php if ($plastictheme_enable_upper_left): ?>
              <?php if ($plastictheme_enable_upper_right): ?>
                <span class="roundcorner-footer-message-top"><span class="roundcorner1"></span><span class="roundcorner2"></span><span class="roundcorner3"></span><span class="roundcorner4"></span></span>
              <?php else: ?>
                <span class="roundcorner-footer-message-top"><span class="roundcorner-left1"></span><span class="roundcorner-left2"></span><span class="roundcorner-left3"></span><span class="roundcorner-left4"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_upper_right): ?>
              <span class="roundcorner-footer-message-top"><span class="roundcorner-right1"></span><span class="roundcorner-right2"></span><span class="roundcorner-right3"></span><span class="roundcorner-right4"></span></span>
            <?php endif; ?>
          <?php endif; ?>

          <div id="footer-message"><?php print $footer_message; ?></div>

          <?php if ($plastictheme_enable_rounded_corners): ?>
            <?php if ($plastictheme_enable_lower_left): ?>
              <?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-footer-message-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              <?php else: ?>
                <span class="roundcorner-footer-message-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-footer-message-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            <?php endif; ?>
          <?php endif; ?>
        <?php endif; ?>

        <?php print $footer; ?>
      </div></div> <!-- /.section, /#footer -->
    <?php endif; ?>
  </div></div> <!-- /#page, /#page-wrapper -->

  <?php print $page_closure; ?>

  <?php print $closure; ?>
</body>
</html>
