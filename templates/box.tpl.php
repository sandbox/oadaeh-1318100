<?php
// $Id: box.tpl.php,v 1.3 2007/12/16 21:01:45 goba Exp $

/**
 * @file box.tpl.php
 *
 * Theme implementation to display a box.
 *
 * Available variables:
 * - $title: Box title.
 * - $content: Box content.
 *
 * @see template_preprocess()
 */
?>
<div class="box">

<?php if ($title): ?>
  <h2><?php print $title ?></h2>
<?php endif; ?>

  <div class="content"><?php print $content ?></div>


        <?php if ($plastictheme_enable_rounded_corners && $plastictheme_content_background): ?>
          <!--?php if (!arg(0) ||
                   (arg(0) == 'node' && arg(1) == 'add') ||
                   (!in_array(arg(0), $content_pages))): ?>
            < ?php if ($plastictheme_enable_lower_left): ?>
              < ?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-content-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              < ?php else: ?>
                <span class="roundcorner-content-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              < ?php endif; ?>
            < ?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-content-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            < ?php endif; ?>
          < ?php elseif (arg(0) == 'node' && arg(2)): ?>
            < ?php if ($plastictheme_enable_lower_left): ?>
              < ?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-node-edit-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              < ?php else: ?>
                <span class="roundcorner-node-edit-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              < ?php endif; ?>
            < ?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-node-edit-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            < ?php endif; ?-->
          <!--?php elseif (arg(0) == 'comment' && arg(1) == 'reply' && $title != 'Preview comment'): ?>
          <!--?php elseif (arg(0) == 'comment' && arg(1) == 'reply'): ?-->
          <?php if (arg(0) == 'comment'): ?>
            <?php if ($plastictheme_enable_lower_left): ?>
              <?php if ($plastictheme_enable_lower_right): ?>
                <span class="roundcorner-comment-reply-bottom"><span class="roundcorner4"></span><span class="roundcorner3"></span><span class="roundcorner2"></span><span class="roundcorner1"></span></span>
              <?php else: ?>
                <span class="roundcorner-comment-reply-bottom"><span class="roundcorner-left4"></span><span class="roundcorner-left3"></span><span class="roundcorner-left2"></span><span class="roundcorner-left1"></span></span>
              <?php endif; ?>
            <?php elseif ($plastictheme_enable_lower_right): ?>
              <span class="roundcorner-comment-reply-bottom"><span class="roundcorner-right4"></span><span class="roundcorner-right3"></span><span class="roundcorner-right2"></span><span class="roundcorner-right1"></span></span>
            <?php endif; ?>
          <?php endif; ?>
        <?php endif; ?>
</div>
