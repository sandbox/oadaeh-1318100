$Id:

Copyright (C) 2006
Author: Jason Flatt <jason@oadaeh.net>
http://www.oadaeh.net/

========================================================================

This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; version 2 of the License.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA

===========================================================================

The theme is based on the Foundation theme [0], developed by Rowan Kerr [1]
of Standard Interactive [2], along with inspiration from the Goofy theme
[3], the Argeebee theme [4] (developed by B�r Kessels [5]), various sites
around the Internet (including, but not necessarily limited to: [6], [7] &
[8]) and my own thoughts and ideas (see BACKGROUND.txt for those).

[0] http://drupal.org/project/foundation
[1] http://drupal.org/user/20129
[2] http://www.standardinteractive.com/
[3] http://drupal.org/project/goofy
[4] http://drupal.org/project/argeebee
[5] http://drupal.org/user/2663
[6] http://www.spiffycorners.com/
[7] http://www.html.it/articoli/nifty/index.html
[8] http://www.acko.net/blog/anti-aliased-nifty-corners

===========================================================================

The main goals for this theme (in the order I remembered them) are these:
 * Rounded corners on the various blocks w/o the use of images or javascript.
 * Being able to take a theme and change only the colors and/or the fonts
   w/o having to wade through the layout.
 * Making it flexible enough to modify w/o a lot of unnecessary work.
 * Not including CSS for modules that are not installed.
