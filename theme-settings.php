<?php
// $Id$

// Include the definition of zen_settings() and zen_theme_get_default_settings().
include_once './' . drupal_get_path('theme', 'zen') . '/theme-settings.php';


/**
 * Implementation of hook_settings() function.
 *
 * @param $saved_settings
 *   An array of saved settings for this theme.
 * @return
 *   A form array.
 */
function plastictheme_settings($saved_settings) {
  // Get the default values from the .info file.
  $defaults = zen_theme_get_default_settings('plastictheme');
  // Merge the saved variables and their default values.
  $settings = array_merge($defaults, $saved_settings);

  /*
   * Create the form using Forms API: http://api.drupal.org/api/6
   */
  $form = array();

  // Fixed or fluid width layout option.
/*
  $form['plastictheme_layout'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Layout'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  $form['plastictheme_layout']['plastictheme_fixed_liquid'] = array(
*/
  $form['plastictheme_fixed_liquid'] = array(
    '#type'          => 'radios',
    '#title'         => t('Specify whether the layout should be fixed or fluid width'),
    '#options'       => array('fixed' => 'Fixed', 'liquid' => 'Liquid'),
    '#default_value' => $settings['plastictheme_fixed_liquid'] ? $settings['plastictheme_fixed_liquid'] : 'liquid',
  );
/*
  $form['plastictheme_layout']['plastictheme_fixed_width'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Width'),
    '#default_value' => $settings['plastictheme_fixed_width'],
    '#size'          => 12,
    '#description'   => t('If selecting fixed width above, specify the width in pixels here.'),
  );
*/

  // Font options.
  $font_families = array(
    "Defaults (the ones already specified in the style sheets)",
    "Times New Roman, Times, Georgia, DejaVu Serif, serif",
    "Times, Times New Roman, Georgia, DejaVu Serif, serif",
    "Georgia, Times New Roman, DejaVu Serif, serif",
    "Verdana, Tahoma, DejaVu Sans, sans-serif",
    "Tahoma, Verdana, DejaVu Sans, sans-serif",
    "Helvetica, Arial, Nimbus Sans L, sans-serif",
    "Arial, Helvetica, Nimbus Sans L, sans-serif",
    "Courier New, DejaVu Sans Mono, monospace",
  );
  $form['plastictheme_font_family'] = array(
    '#type'          => 'radios',
    '#title'         => t('Specify the font family to use'),
    '#default_value' => $settings['plastictheme_font_family'],
    '#options'       => $font_families,
    '#description'   => t("Choose the font family to use in the theme."),
  );

  // Header height options.
  $form['plastictheme_header_height'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Header height'),
    '#default_value' => $settings['plastictheme_header_height'],
    '#size'          => 12,
    '#description'   => t('This is the height, in pixels or ems, that the size of the area above the content should be. Enter a positive number here (immediately followed by either "px" or "em" -- no spaces) to set the size.'),
  );

  // Rounded corner options.
  $form['plastictheme_enable_rounded_corners'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Enable rounded corners'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t("Choose whether to enable or disable rounded corners."),
  );
//  $form['plastictheme_enable_rounded_corners']['plastictheme_upper_left'] = array(
//    '#type'   => 'fieldset',
//    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 15px; width: 45%;">',
//    '#suffix'        => '</span>',
//  );
  $form['plastictheme_enable_rounded_corners']['plastictheme_upper_left']['plastictheme_enable_upper_left'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable the upper-left corner'),
    '#default_value' => $settings['plastictheme_enable_upper_left'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 15px; width: 45%;">',
    '#suffix'        => '</span>',
  );
//  $form['plastictheme_enable_rounded_corners']['plastictheme_upper_right'] = array(
//    '#type'   => 'fieldset',
//    '#prefix'        => '<span style="float: right; margin-left: 15px; width: 45%;">',
//    '#suffix'        => '</span></div>',
//  );
  $form['plastictheme_enable_rounded_corners']['plastictheme_upper_right']['plastictheme_enable_upper_right'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable the upper-right corner'),
    '#default_value' => $settings['plastictheme_enable_upper_right'],
    '#prefix'        => '<span style="float: right; margin-left: 15px; width: 45%;">',
    '#suffix'        => '</span></div>',
  );
//  $form['plastictheme_enable_rounded_corners']['plastictheme_lower_left'] = array(
//    '#type'   => 'fieldset',
//    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 15px; width: 45%;">',
//    '#suffix'        => '</span>',
//  );
  $form['plastictheme_enable_rounded_corners']['plastictheme_lower_left']['plastictheme_enable_lower_left'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable the lower-left corner'),
    '#default_value' => $settings['plastictheme_enable_lower_left'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 15px; width: 45%;">',
    '#suffix'        => '</span>',
  );
//  $form['plastictheme_enable_rounded_corners']['plastictheme_lower_right'] = array(
//    '#type'   => 'fieldset',
//    '#prefix'        => '<span style="float: right; margin-left: 15px; width: 45%;">',
//    '#suffix'        => '</span></div>',
//  );
  $form['plastictheme_enable_rounded_corners']['plastictheme_lower_right']['plastictheme_enable_lower_right'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable the lower-right corner'),
    '#default_value' => $settings['plastictheme_enable_lower_right'],
    '#prefix'        => '<span style="float: right; margin-left: 15px; width: 45%;">',
    '#suffix'        => '</span></div>',
  );

/*
  // Border options.
  $form['plastictheme_enable_borders'] = array(
    '#type'          => 'checkbox',
    '#title'         => t('Enable borders'),
    '#default_value' => $settings['plastictheme_enable_borders'],
    '#description'   => t("Choose whether to enable or disable borders."),
  );
*/

  // Color options.
  $description  = t('Change the various colors below to customize the appearance of Plastic Theme. Enter the color values as either <a href="http://en.wikipedia.org/wiki/Web_colors">named colors</a> or using the hex triplet form of #RRGGBB. You can use http://www.colorpicker.com/ (or any other color picker software) to help select the color values you want to use.');
  $description .= '<br /><br />'. t('There are three ways to use this area:') .'<ol>';
  $description .= '<li>'. t('Leave all of the fields empty to use the colors specified in the CSS files (spcifically, the colors.css file). (If you are using the Colorpicker module, fields with only a # in them will be ignored.)') .'</li>';
  $description .= '<li>'. t('Leave the Default color field empty, but fill in other fields you want to change. The colors not specified will be pulled from the CSS files.') .'</li>';
  $description .= '<li>'. t("Specify a Default color and whatever other colors you want to change. For every color below the Default color you specify, that color will be used. Otherwise, the Default color will be used.") .'</li>';
  $description .= '</ol>';
  $form['plastictheme_color_selections'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Color selections'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => $description,
  );

  // Check for the existance of the Colorpicker module and use it instead of a
  // normal textfield, if it is installed and enabled.
  $color_field_type = (function_exists('colorpicker_2_or_later')) ? 'colorpicker_textfield' : 'textfield';

  // The default color. This should make setting a base color easier.
  $form['plastictheme_color_selections']['plastictheme_default_color'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Default color'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_default_color'],
    '#description'   => t('This is the default color that can be chosen to represent any of the elements below. Specify the most common color of your theme here and leave any field below blank that you want to use this color.'),
  );

  // Basic page element colors.
  $form['plastictheme_color_selections']['plastictheme_page_elements'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Page elements'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t('These are the various elemets of a page.'),
  );
  // Base page colors.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_page_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Page background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_page_background'],
    '#prefix'        => '<div style="float: left; margin-left: 0.2em; width: 49%;">',
    '#suffix'        => '</div>',
  );
  // Content not defined elsewhere (forms, admin pages, lists, etc.)
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_content_title'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Default content title text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_content_title'],
    '#description'   => t('This is the title color for all content that is not specified elsewhere on this form.'),
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_content_title_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Default content title background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_content_title_background'],
    '#description'   => t('This is the title background color for all content that is not specified elsewhere on this form.'),
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_page_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Default content<br />text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_page_text'],
    '#description'   => t('All text on this site will be this color, unless changed by one of the settings below.'),
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_content_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Default content background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_content_background'],
    '#description'   => t('This is the background color for all content that is not specified elsewhere on this form.'),
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );
  // Header.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_header_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Header text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_header_text'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_header_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Header background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_header_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  // Mission.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_mission_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Mission text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_mission_text'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_mission_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Mission background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_mission_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );
  // Help.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_help_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Help text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_help_text'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_help_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Help background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_help_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  // Footer.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_footer_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Footer text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_footer_text'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_footer_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Footer background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_footer_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );

  // Links.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Hyperlink elements'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t('These are the colors for the various states of standard hyperlinks.'),
    '#prefix'        => '<div style="clear: both;"></div>',
  );
  // Unvisited links.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_link_color'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Link text<br />(unvisited)'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_link_color'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_link_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Link background<br />(unvisited)'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_link_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  // Visited links.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_visited_color'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Visited link<br />text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_visited_color'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_visited_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Visited link<br />background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_visited_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );
  // Hovered links.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_hover_color'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Hovered link<br />text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_hover_color'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_hover_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Hovered link<br />background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_hover_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  // Focused links.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_focus_color'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Focused link<br />text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_focus_color'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_focus_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Focused link<br />background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_focus_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );
  // Active links.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_active_color'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Active link<br />text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_active_color'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_links']['plastictheme_active_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Active links<br />background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_active_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );

  // Table background colors.
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_table_elements'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Table elements'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_table_elements']['plastictheme_odd_row_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Odd numbered row background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_odd_row_background'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 32%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_table_elements']['plastictheme_even_row_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Even numbered row background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_even_row_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 32%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_page_elements']['plastictheme_table_elements']['plastictheme_selected_column_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Selected column background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_selected_column_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 32%;">',
    '#suffix'        => '</span></div>',
  );

  // Block colors.
  $form['plastictheme_color_selections']['plastictheme_block_elements'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Block elements'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t('These are the various elemets of blocks.'),
  );
  $form['plastictheme_color_selections']['plastictheme_block_elements']['plastictheme_block_title'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_block_title'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_block_elements']['plastictheme_block_title_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_block_title_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_block_elements']['plastictheme_block_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_block_text'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_block_elements']['plastictheme_block_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_block_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );

  // Node colors.
  $form['plastictheme_color_selections']['plastictheme_node_elements'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Node elements'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t('These are the various elemets of nodes.'),
  );
  $form['plastictheme_color_selections']['plastictheme_node_elements']['plastictheme_node_title'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_node_title'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_node_elements']['plastictheme_node_title_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_node_title_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_node_elements']['plastictheme_node_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_node_text'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_node_elements']['plastictheme_node_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content background'),
    '#default_value' => $settings['plastictheme_node_background'],
    '#size'          => 12,
//    '#description'   => t('This color may also be used for other areas that are not specified here.'),
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );

  // Sticky node colors.
  $form['plastictheme_color_selections']['plastictheme_sticky_elements'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Sticky node elements'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t('These are the various elemets of sticky nodes.'),
  );
  $form['plastictheme_color_selections']['plastictheme_sticky_elements']['plastictheme_sticky_title'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_sticky_title'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_sticky_elements']['plastictheme_sticky_title_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_sticky_title_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_sticky_elements']['plastictheme_sticky_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_sticky_text'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_sticky_elements']['plastictheme_sticky_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_sticky_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );

  // Comment colors.
  if (module_exists(comment)) {
    $form['plastictheme_color_selections']['plastictheme_comment_elements'] = array(
      '#type'        => 'fieldset',
      '#title'       => t('Comment elements'),
      '#collapsible' => TRUE,
      '#collapsed'   => FALSE,
      '#description' => t("These are the various elemets of comments."),
    );
    $form['plastictheme_color_selections']['plastictheme_comment_elements']['plastictheme_comment_title'] = array(
      '#type'          => $color_field_type,
      '#title'         => t('Title text'),
      '#size'          => 12,
      '#default_value' => $settings['plastictheme_comment_title'],
      '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
      '#suffix'        => '</span>',
    );
    $form['plastictheme_color_selections']['plastictheme_comment_elements']['plastictheme_comment_title_background'] = array(
      '#type'          => $color_field_type,
      '#title'         => t('Title background'),
      '#size'          => 12,
      '#default_value' => $settings['plastictheme_comment_title_background'],
      '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
      '#suffix'        => '</span>',
    );
    $form['plastictheme_color_selections']['plastictheme_comment_elements']['plastictheme_comment_text'] = array(
      '#type'          => $color_field_type,
      '#title'         => t('Content text'),
      '#size'          => 12,
      '#default_value' => $settings['plastictheme_comment_text'],
      '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
      '#suffix'        => '</span>',
    );
    $form['plastictheme_color_selections']['plastictheme_comment_elements']['plastictheme_comment_background'] = array(
      '#type'          => $color_field_type,
      '#title'         => t('Content background'),
      '#size'          => 12,
      '#default_value' => $settings['plastictheme_comment_background'],
      '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
      '#suffix'        => '</span></div>',
    );
  }

  // Administration colors.
  $form['plastictheme_color_selections']['plastictheme_admin_elements'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Administration elements'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
    '#description' => t("These are the various elemets of administration."),
  );
  $form['plastictheme_color_selections']['plastictheme_admin_elements']['plastictheme_admin_title'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_admin_title'],
    '#prefix'        => '<div style="clear: both;"><span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_admin_elements']['plastictheme_admin_title_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Title background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_admin_title_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_admin_elements']['plastictheme_admin_content_text'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content text'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_admin_content_text'],
    '#prefix'        => '<span style="float: left; margin-right: 0.2em; width: 24%;">',
    '#suffix'        => '</span>',
  );
  $form['plastictheme_color_selections']['plastictheme_admin_elements']['plastictheme_admin_content_background'] = array(
    '#type'          => $color_field_type,
    '#title'         => t('Content background'),
    '#size'          => 12,
    '#default_value' => $settings['plastictheme_admin_content_background'],
    '#prefix'        => '<span style="float: left; margin-left: 0.2em; width: 24%;">',
    '#suffix'        => '</span></div>',
  );

  // Add the base theme's settings.
  $form += zen_settings($saved_settings, $defaults);
  // Remove some of the base theme's settings.
  unset($form['themedev']['zen_layout']); // We don't need to select the base stylesheet.
  // Return the form
  return $form;
}
