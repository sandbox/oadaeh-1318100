<?php
// $Id$

/**
 * @file
 * Contains theme override functions and preprocess functions for the theme.
 *
 * ABOUT THE TEMPLATE.PHP FILE
 *
 *   The template.php file is one of the most useful files when creating or
 *   modifying Drupal themes. You can add new regions for block content, modify
 *   or override Drupal's theme functions, interoundcornerept or make additional
 *   variables available to your theme, and create custom PHP logic. For more
 *   information, please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/theme-guide
 *
 * OVERRIDING THEME FUNCTIONS
 *
 *   The Drupal theme system uses special theme functions to generate HTML
 *   output automatically. Often we wish to customize this HTML output. To do
 *   this, we have to override the theme function. You have to first find the
 *   theme function that generates the output, and then "catch" it and modify it
 *   here. The easiest way to do it is to copy the original function in its
 *   entirety and paste it here, changing the prefix from theme_ to plastictheme_.
 *   For example:
 *
 *     original: theme_breadcrumb()
 *     theme override: plastictheme_breadcrumb()
 *
 *   where plastictheme is the name of your sub-theme. For example, the
 *   zen_classic theme would define a zen_classic_breadcrumb() function.
 *
 *   If you would like to override any of the theme functions used in Zen core,
 *   you should first look at how Zen core implements those functions:
 *     theme_breadcrumbs()      in zen/template.php
 *     theme_menu_item_link()   in zen/template.php
 *     theme_menu_local_tasks() in zen/template.php
 *
 *   For more information, please visit the Theme Developer's Guide on
 *   Drupal.org: http://drupal.org/node/173880
 *
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *   Each tpl.php template file has several variables which hold various pieces
 *   of content. You can modify those variables (or add new ones) before they
 *   are used in the template files by using preprocess functions.
 *
 *   This makes THEME_preprocess_HOOK() functions the most powerful functions
 *   available to themers.
 *
 *   It works by having one preprocess function for each template file or its
 *   derivatives (called template suggestions). For example:
 *     THEME_preprocess_page    alters the variables for page.tpl.php
 *     THEME_preprocess_node    alters the variables for node.tpl.php or
 *                              for node-forum.tpl.php
 *     THEME_preprocess_comment alters the variables for comment.tpl.php
 *     THEME_preprocess_block   alters the variables for block.tpl.php
 *
 *   For more information on preprocess functions and template suggestions,
 *   please visit the Theme Developer's Guide on Drupal.org:
 *   http://drupal.org/node/223440
 *   and http://drupal.org/node/190815#template-suggestions
 */


/**
 * Implementation of hook_theme().
 */
function plastictheme_theme(&$existing, $type, $theme, $path) {
  $hooks = zen_theme($existing, $type, $theme, $path);
  // Add your theme hooks like this:
  /*
  $hooks['hook_name_here'] = array( // Details go here );
  */
  // @TODO: Needs detailed comments. Patches welcome!
  return $hooks;
}


/**
 * Override or insert variables into all templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered (name of the .tpl.php file.)
 */
function plastictheme_preprocess(&$vars, $hook) {
  // Save the theme settings into a local array, so we aren't calling the
  // function so much.
  $theme_settings = theme_get_settings('plastictheme');

  // Set the layout type.
  if ($theme_settings['plastictheme_fixed_liquid'] == 'fixed') {
    drupal_add_css(drupal_get_path('theme', 'plastictheme') . '/css/layout-fixed.css', 'theme', 'all');
  }
  else {
    drupal_add_css(drupal_get_path('theme', 'plastictheme') . '/css/layout-liquid.css', 'theme', 'all');
  }

  // Set rounded corners.
  $vars['plastictheme_enable_rounded_corners'] = FALSE;

  if ($theme_settings['plastictheme_enable_upper_left']) {
    $vars['plastictheme_enable_rounded_corners'] = TRUE;
    $vars['plastictheme_enable_upper_left'] = $theme_settings['plastictheme_enable_upper_left'];
  }
  if ($theme_settings['plastictheme_enable_upper_right']) {
    $vars['plastictheme_enable_rounded_corners'] = TRUE;
    $vars['plastictheme_enable_upper_right'] = $theme_settings['plastictheme_enable_upper_right'];
  }
  if ($theme_settings['plastictheme_enable_lower_left']) {
    $vars['plastictheme_enable_rounded_corners'] = TRUE;
    $vars['plastictheme_enable_lower_left'] = $theme_settings['plastictheme_enable_lower_left'];
  }
  if ($theme_settings['plastictheme_enable_lower_right']) {
    $vars['plastictheme_enable_rounded_corners'] = TRUE;
    $vars['plastictheme_enable_lower_right'] = $theme_settings['plastictheme_enable_lower_right'];
  }

  // Set borders.
//  $vars['plastictheme_enable_borders'] = $theme_settings['plastictheme_enable_borders'];
}


/**
 * Override or insert variables into the page templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
function plastictheme_preprocess_page(&$vars, $hook) {
  // Save the theme settings into a local array, so we aren't calling the
  // function so much.
  $theme_settings = theme_get_settings('plastictheme');

  // Font familes.
  $font_family['plastictheme_font_family'] = $theme_settings['plastictheme_font_family'];
  if ($font_family['plastictheme_font_family'] > 0) {
    $vars['plastictheme_font_family'] = "<style type=\"text/css\">\n";

    switch ($font_family['plastictheme_font_family']) {
      case 1:
        $vars['plastictheme_font_family'] .= "body { font-family: Times New Roman, Times, Georgia, DejaVu Serif, serif; }\n";
        break;
      case 2:
        $vars['plastictheme_font_family'] .= "body { font-family: Times, Times New Roman, Georgia, DejaVu Serif, serif; }\n";
        break;
      case 3:
        $vars['plastictheme_font_family'] .= "body { font-family: Georgia, Times New Roman, DejaVu Serif, serif; }\n";
        break;
      case 4:
        $vars['plastictheme_font_family'] .= "body { font-family: Verdana, Tahoma, DejaVu Sans, sans-serif; }\n";
        break;
      case 5:
        $vars['plastictheme_font_family'] .= "body { font-family: Tahoma, Verdana, DejaVu Sans, sans-serif; }\n";
        break;
      case 6:
        $vars['plastictheme_font_family'] .= "body { font-family: Helvetica, Arial, Nimbus Sans L, sans-serif; }\n";
        break;
      case 7:
        $vars['plastictheme_font_family'] .= "body { font-family: Arial, Helvetica, Nimbus Sans L, sans-serif; }\n";
        break;
      case 8:
        $vars['plastictheme_font_family'] .= "body { font-family: Courier New, DejaVu Sans Mono, monospace; }\n";
        break;
    }

    $vars['plastictheme_font_family'] .= "</style>";
  }


  // Custom colors.
  $custom_colors = array();

  $custom_colors['plastictheme_default_color'] = $theme_settings['plastictheme_default_color'];

  $custom_colors['plastictheme_page_text'] = $theme_settings['plastictheme_page_text'];
  $custom_colors['plastictheme_page_background'] = $theme_settings['plastictheme_page_background'];
  $custom_colors['plastictheme_header_text'] = $theme_settings['plastictheme_header_text'];
  $custom_colors['plastictheme_header_background'] = $theme_settings['plastictheme_header_background'];
  $custom_colors['plastictheme_mission_text'] = $theme_settings['plastictheme_mission_text'];
  $custom_colors['plastictheme_mission_background'] = $theme_settings['plastictheme_mission_background'];
  $custom_colors['plastictheme_help_text'] = $theme_settings['plastictheme_help_text'];
  $custom_colors['plastictheme_help_background'] = $theme_settings['plastictheme_help_background'];
  $custom_colors['plastictheme_footer_text'] = $theme_settings['plastictheme_footer_text'];
  $custom_colors['plastictheme_footer_background'] = $theme_settings['plastictheme_footer_background'];
  $custom_colors['plastictheme_content_title'] = $theme_settings['plastictheme_content_title'];
  $custom_colors['plastictheme_content_title_background'] = $theme_settings['plastictheme_content_title_background'];
  $custom_colors['plastictheme_content_background'] = $theme_settings['plastictheme_content_background'];

  $custom_colors['plastictheme_link_color'] = $theme_settings['plastictheme_link_color'];
  $custom_colors['plastictheme_link_background'] = $theme_settings['plastictheme_link_background'];
  $custom_colors['plastictheme_visited_color'] = $theme_settings['plastictheme_visited_color'];
  $custom_colors['plastictheme_visited_background'] = $theme_settings['plastictheme_visited_background'];
  $custom_colors['plastictheme_hover_color'] = $theme_settings['plastictheme_hover_color'];
  $custom_colors['plastictheme_hover_background'] = $theme_settings['plastictheme_hover_background'];
  $custom_colors['plastictheme_focus_color'] = $theme_settings['plastictheme_focus_color'];
  $custom_colors['plastictheme_focus_background'] = $theme_settings['plastictheme_focus_background'];
  $custom_colors['plastictheme_active_color'] = $theme_settings['plastictheme_active_color'];
  $custom_colors['plastictheme_active_background'] = $theme_settings['plastictheme_active_background'];

  $custom_colors['plastictheme_odd_row_background'] = $theme_settings['plastictheme_odd_row_background'];
  $custom_colors['plastictheme_even_row_background'] = $theme_settings['plastictheme_even_row_background'];
  $custom_colors['plastictheme_selected_column_background'] = $theme_settings['plastictheme_selected_column_background'];

  $custom_colors['plastictheme_block_title'] = $theme_settings['plastictheme_block_title'];
  $custom_colors['plastictheme_block_title_background'] = $theme_settings['plastictheme_block_title_background'];
  $custom_colors['plastictheme_block_text'] = $theme_settings['plastictheme_block_text'];
  $custom_colors['plastictheme_block_background'] = $theme_settings['plastictheme_block_background'];

  $custom_colors['plastictheme_node_title'] = $theme_settings['plastictheme_node_title'];
  $custom_colors['plastictheme_node_title_background'] = $theme_settings['plastictheme_node_title_background'];
  $custom_colors['plastictheme_node_text'] = $theme_settings['plastictheme_node_text'];
  $custom_colors['plastictheme_node_background'] = $theme_settings['plastictheme_node_background'];

  $custom_colors['plastictheme_sticky_title'] = $theme_settings['plastictheme_sticky_title'];
  $custom_colors['plastictheme_sticky_title_background'] = $theme_settings['plastictheme_sticky_title_background'];
  $custom_colors['plastictheme_sticky_text'] = $theme_settings['plastictheme_sticky_text'];
  $custom_colors['plastictheme_sticky_background'] = $theme_settings['plastictheme_sticky_background'];

  $custom_colors['plastictheme_comment_title'] = $theme_settings['plastictheme_comment_title'];
  $custom_colors['plastictheme_comment_title_background'] = $theme_settings['plastictheme_comment_title_background'];
  $custom_colors['plastictheme_comment_text'] = $theme_settings['plastictheme_comment_text'];
  $custom_colors['plastictheme_comment_background'] = $theme_settings['plastictheme_comment_background'];

  $custom_colors['plastictheme_admin_title'] = $theme_settings['plastictheme_admin_title'];
  $custom_colors['plastictheme_admin_title_background'] = $theme_settings['plastictheme_admin_title_background'];
  $custom_colors['plastictheme_admin_content_text'] = $theme_settings['plastictheme_admin_content_text'];
  $custom_colors['plastictheme_admin_content_background'] = $theme_settings['plastictheme_admin_content_background'];

  foreach($custom_colors as $value) {
    if ($value) {
      if ($custom_colors['plastictheme_default_color']) {
        global $theme_path;
        $css = drupal_add_css();
        unset($css['all']['theme'][$theme_path .'/css/colors.css']);
        $vars['styles'] = drupal_get_css($css);
      }

      $vars['plastictheme_custom_colors'] = "<style type=\"text/css\">\n";

      // Base page.
      if ($custom_colors['plastictheme_page_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_page_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "body, .breadcrumb, #content, #header, #footer, #message status, #navigation, .page, #page, #page-wrapper, .preview, .region-sidebar-first, .region-sidebar-second, .roundcorner-block-bottom, .roundcorner-block-title-bottom, .roundcorner-block-title-top, .roundcorner-block-top, .roundcorner-comment-bottom, .roundcorner-comment-reply-bottom, .roundcorner-comment-title-bottom, .roundcorner-comment-title-top, .roundcorner-comment-top, .roundcorner-footer-bottom, .roundcorner-footer-top, .roundcorner-header-top, .roundcorner-header-bottom, .roundcorner-mission-top, .roundcorner-mission-bottom, .roundcorner-node-bottom, .roundcorner-node-edit-bottom, .roundcorner-node-title-bottom, .roundcorner-node-title-top, .roundcorner-node-top, .roundcorner-page-opener-top, .roundcorner-page-opener-bottom, .roundcorner-sticky-bottom, .roundcorner-sticky-title-bottom, .roundcorner-sticky-title-top, .roundcorner-sticky-top, .roundcorner-title-top, .roundcorner-title-bottom, .two-sidebars #content .section { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_page_text'] ? $custom_colors['plastictheme_page_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_page_background'] ? $custom_colors['plastictheme_page_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "body, .breadcrumb, #content, #header, #footer, #message status, #navigation, .page, #page, #page-wrapper, .preview, .region-sidebar-first, .region-sidebar-second, .roundcorner-block-bottom, .roundcorner-block-title-bottom, .roundcorner-block-title-top, .roundcorner-block-top, .roundcorner-comment-bottom, .roundcorner-comment-reply-bottom, .roundcorner-comment-title-bottom, .roundcorner-comment-title-top, .roundcorner-comment-top, .roundcorner-footer-bottom, .roundcorner-footer-top, .roundcorner-header-top, .roundcorner-header-bottom, .roundcorner-mission-top, .roundcorner-mission-bottom, .roundcorner-node-bottom, .roundcorner-node-edit-bottom, .roundcorner-node-title-bottom, .roundcorner-node-title-top, .roundcorner-node-top, .roundcorner-page-opener-top, .roundcorner-page-opener-bottom, .roundcorner-sticky-bottom, .roundcorner-sticky-title-bottom, .roundcorner-sticky-title-top, .roundcorner-sticky-top, .roundcorner-title-top, .roundcorner-title-bottom, .two-sidebars #content .section { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_page_text'] ? $custom_colors['plastictheme_page_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_page_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "body, .breadcrumb, #content, #header, #footer, #message status, #navigation, .page, #page, #page-wrapper, .preview, .region-sidebar-first, .region-sidebar-second, .roundcorner-block-bottom, .roundcorner-block-title-bottom, .roundcorner-block-title-top, .roundcorner-block-top, .roundcorner-comment-bottom, .roundcorner-comment-reply-bottom, .roundcorner-comment-title-bottom, .roundcorner-comment-title-top, .roundcorner-comment-top, .roundcorner-footer-bottom, .roundcorner-footer-top, .roundcorner-header-top, .roundcorner-header-bottom, .roundcorner-mission-top, .roundcorner-mission-bottom, .roundcorner-node-bottom, .roundcorner-node-edit-bottom, .roundcorner-node-title-bottom, .roundcorner-node-title-top, .roundcorner-node-top, .roundcorner-page-opener-top, .roundcorner-page-opener-bottom, .roundcorner-sticky-bottom, .roundcorner-sticky-title-bottom, .roundcorner-sticky-title-top, .roundcorner-sticky-top, .roundcorner-title-top, .roundcorner-title-bottom, .two-sidebars #content .section { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_page_background'] ? $custom_colors['plastictheme_page_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Header.
      if ($custom_colors['plastictheme_header_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_header_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .=".header, .header-bottom, .header-bottom-left, .header-bottom-right, .roundcorner-header-bottom *, .roundcorner-header-content, .roundcorner-header-top *, #page-opener, .page-opener-bottom, .page-opener-bottom-left, .page-opener-bottom-right, .roundcorner-page-opener-top *, .roundcorner-page-opener-content, .roundcorner-page-opener-bottom *, #main-menu, .roundcorner-main-menu-top *, .roundcorner-main-menu-content, .roundcorner-main-menu-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_header_text'] ? $custom_colors['plastictheme_header_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_header_background'] ? $custom_colors['plastictheme_header_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .=".header, .header-bottom, .header-bottom-left, .header-bottom-right, .roundcorner-header-bottom *, .roundcorner-header-content, .roundcorner-header-top *, #page-opener, .page-opener-bottom, .page-opener-bottom-left, .page-opener-bottom-right, .roundcorner-page-opener-top *, .roundcorner-page-opener-content, .roundcorner-page-opener-bottom *, #main-menu, .roundcorner-main-menu-top *, .roundcorner-main-menu-content, .roundcorner-main-menu-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_header_text'] ? $custom_colors['plastictheme_header_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_header_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .=".header, .header-bottom, .header-bottom-left, .header-bottom-right, .roundcorner-header-bottom *, .roundcorner-header-content, .roundcorner-header-top *, #page-opener, .page-opener-bottom, .page-opener-bottom-left, .page-opener-bottom-right, .roundcorner-page-opener-top *, .roundcorner-page-opener-content, .roundcorner-page-opener-bottom *, #main-menu, .roundcorner-main-menu-top *, .roundcorner-main-menu-content, .roundcorner-main-menu-bottom * { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_header_background'] ? $custom_colors['plastictheme_header_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Mission.
      if ($custom_colors['plastictheme_mission_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_mission_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "#mission, .roundcorner-mission-top *, .roundcorner-mission-content, .roundcorner-mission-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_mission_text'] ? $custom_colors['plastictheme_mission_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_mission_background'] ? $custom_colors['plastictheme_mission_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "#mission, .roundcorner-mission-top *, .roundcorner-mission-content, .roundcorner-mission-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_mission_text'] ? $custom_colors['plastictheme_mission_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_mission_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "#mission, .roundcorner-mission-top *, .roundcorner-mission-content, .roundcorner-mission-bottom * { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_mission_background'] ? $custom_colors['plastictheme_mission_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Help.
      if ($custom_colors['plastictheme_help_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_help_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= ".help { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_help_text'] ? $custom_colors['plastictheme_help_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; ";
          $vars['plastictheme_custom_colors'] .= "background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_help_background'] ? $custom_colors['plastictheme_help_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= ".help { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_help_text'] ? $custom_colors['plastictheme_help_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_help_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".help { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_help_background'] ? $custom_colors['plastictheme_help_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Footer.
      if ($custom_colors['plastictheme_footer_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_footer_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= ".footer, .roundcorner-footer-bottom *, .roundcorner-footer-content, .roundcorner-footer-top *, #footer-message, .roundcorner-footer-message-top *, .roundcorner-footer-message-content, .roundcorner-footer-message-bottom *, #secondary-menu, .roundcorner-secondary-menu-top *, .roundcorner-secondary-menu-content, .roundcorner-secondary-menu-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_footer_text'] ? $custom_colors['plastictheme_footer_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_footer_background'] ? $custom_colors['plastictheme_footer_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= ".footer, .roundcorner-footer-bottom *, .roundcorner-footer-content, .roundcorner-footer-top *, #footer-message, .roundcorner-footer-message-top *, .roundcorner-footer-message-content, .roundcorner-footer-message-bottom *, #secondary-menu, .roundcorner-secondary-menu-top *, .roundcorner-secondary-menu-content, .roundcorner-secondary-menu-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_footer_text'] ? $custom_colors['plastictheme_footer_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_footer_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".footer, .roundcorner-footer-bottom *, .roundcorner-footer-content, .roundcorner-footer-top *, #footer-message, .roundcorner-footer-message-top *, .roundcorner-footer-message-content, .roundcorner-footer-message-bottom *, #secondary-menu, .roundcorner-secondary-menu-top *, .roundcorner-secondary-menu-content, .roundcorner-secondary-menu-bottom * { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_footer_background'] ? $custom_colors['plastictheme_footer_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Other content.
      // The title text.
      if ($custom_colors['plastictheme_content_title'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".box h2 { color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_content_title'] ? $custom_colors['plastictheme_content_title'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // The title background.
      if ($custom_colors['plastictheme_content_title_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".box h2 { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_content_title_background'] ? $custom_colors['plastictheme_content_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // The content area.
      if ($custom_colors['plastictheme_content_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_content_background']  = "<style type=\"text/css\">\n";
        $vars['plastictheme_content_background'] .= ".box, .box .content, #content-area, .roundcorner-content-bottom *, .roundcorner-content-content, .roundcorner-welcome-bottom * { background-color: ";
        $vars['plastictheme_content_background'] .= $custom_colors['plastictheme_content_background'] ? $custom_colors['plastictheme_content_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_content_background'] .= "; }\n";
        $vars['plastictheme_content_background'] .= "</style>";

        $vars['plastictheme_nonnode_content_background'] = $vars['plastictheme_content_background'];
      }

      // Tables.
      if ($custom_colors['plastictheme_odd_row_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "tr.odd { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_odd_row_background'] ? $custom_colors['plastictheme_odd_row_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      if ($custom_colors['plastictheme_even_row_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "tr.even { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_even_row_background'] ? $custom_colors['plastictheme_even_row_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      if ($custom_colors['plastictheme_selected_column_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "td.active { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_selected_column_background'] ? $custom_colors['plastictheme_selected_column_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Blocks.
      // The title text.
      if ($custom_colors['plastictheme_block_title'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".block .title { color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_block_title'] ? $custom_colors['plastictheme_block_title'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // The title background.
      if ($custom_colors['plastictheme_block_title_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".block .title, .roundcorner-block-title-bottom *, .roundcorner-block-title-top * { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_block_title_background'] ? $custom_colors['plastictheme_block_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // The content area.
      if ($custom_colors['plastictheme_block_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_block_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= ".block, .roundcorner-block-bottom *, .roundcorner-block-top *, .roundcorner-block-content { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_block_text'] ? $custom_colors['plastictheme_block_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_block_background'] ? $custom_colors['plastictheme_block_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= ".block, .roundcorner-block-bottom *, .roundcorner-block-top *, .roundcorner-block-content { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_block_text'] ? $custom_colors['plastictheme_block_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_block_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".block, .roundcorner-block-bottom *, .roundcorner-block-top *, .roundcorner-block-content { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_block_background'] ? $custom_colors['plastictheme_block_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Nodes.
      // The title text.
      if ($custom_colors['plastictheme_node_title'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".title, .title a:link, .title a:visited { color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_title'] ? $custom_colors['plastictheme_node_title'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_title_background'] ? $custom_colors['plastictheme_node_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";

        if ($custom_colors['plastictheme_node_title_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= ".title a:hover { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_title_background'] ? $custom_colors['plastictheme_node_title_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_title'] ? $custom_colors['plastictheme_node_title'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      // The title background.
      if ($custom_colors['plastictheme_node_title_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".node .tabs, .roundcorner-node-title-bottom *, .roundcorner-node-title-top *, .roundcorner-title-bottom *, .roundcorner-title-top *, .tabs, .title { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_title_background'] ? $custom_colors['plastictheme_node_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // The content area.
      if ($custom_colors['plastictheme_node_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_node_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= ".book, #book-outline, .delete-page-content, #first-time, .node, #node-delete-confirm, .page-content, .preview .node, .preview-page-content form#node-form, .revisions-page-content, .roundcorner-node-bottom *, .roundcorner-node-edit-bottom *, .roundcorner-node-content, .roundcorner-node-top *, .user-page-content { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_text'] ? $custom_colors['plastictheme_node_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_background'] ? $custom_colors['plastictheme_node_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= ".book, #book-outline, .delete-page-content, #first-time, .node, #node-delete-confirm, .page-content, .preview .node, .preview-page-content form#node-form, .revisions-page-content, .roundcorner-node-bottom *, .roundcorner-node-edit-bottom *, .roundcorner-node-content, .roundcorner-node-top *, .user-page-content { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_text'] ? $custom_colors['plastictheme_node_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_node_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".book, #book-outline, .delete-page-content, #first-time, .node, #node-delete-confirm, .node-form, #node-form, .page-content, .preview .node, .preview-page-content form#node-form, .revisions-page-content, .roundcorner-node-bottom *, .roundcorner-node-edit-bottom *, .roundcorner-node-content, .roundcorner-node-top *, .user-page-content { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_node_background'] ? $custom_colors['plastictheme_node_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";

        $vars['plastictheme_node_content_background']  = "<style type=\"text/css\">\n";
        $vars['plastictheme_node_content_background'] .= "#content-area, .roundcorner-content-bottom *, .roundcorner-content-content, .roundcorner-welcome-bottom * { background-color: ";
        $vars['plastictheme_node_content_background'] .= $custom_colors['plastictheme_node_background'] ? $custom_colors['plastictheme_node_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_node_content_background'] .= "; }\n";
        $vars['plastictheme_node_content_background'] .= "</style>";
      }

      // Sticky nodes.
      // The title text.
      if ($custom_colors['plastictheme_sticky_title'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".node-sticky .title, .node-sticky .title a:link, .node-sticky .title a:visited { color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_title'] ? $custom_colors['plastictheme_sticky_title'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_title_background'] ? $custom_colors['plastictheme_sticky_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";

        if ($custom_colors['plastictheme_sticky_title_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= ".node-sticky .title a:hover { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_title_background'] ? $custom_colors['plastictheme_sticky_title_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_title'] ? $custom_colors['plastictheme_sticky_title'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      // The title background.
      if ($custom_colors['plastictheme_sticky_title_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".node-sticky .title, .roundcorner-sticky-title-bottom *, .roundcorner-sticky-title-top *, .sticky .title { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_title_background'] ? $custom_colors['plastictheme_sticky_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // The content area.
      if ($custom_colors['plastictheme_sticky_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_sticky_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "#node-sticky, .node-sticky, .roundcorner-sticky-content, .roundcorner-sticky-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_text'] ? $custom_colors['plastictheme_sticky_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_background'] ? $custom_colors['plastictheme_sticky_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "#node-sticky, .node-sticky, .roundcorner-sticky-content, .roundcorner-sticky-bottom * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_text'] ? $custom_colors['plastictheme_sticky_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_sticky_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "#node-sticky, .node-sticky, .roundcorner-sticky-content, .roundcorner-sticky-bottom * { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_sticky_background'] ? $custom_colors['plastictheme_sticky_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Comments.
      // The title text.
      if ($custom_colors['plastictheme_comment_title'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".comment .title, .comment .title a:link, .comment .title a:visited, #comments .title { color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_title'] ? $custom_colors['plastictheme_comment_title'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_title_background'] ? $custom_colors['plastictheme_comment_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";

        // The title text and background.
        if ($custom_colors['plastictheme_comment_title_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= ".comment .title a:hover { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_title_background'] ? $custom_colors['plastictheme_comment_title_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_title'] ? $custom_colors['plastictheme_comment_title'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      // The title background.
      if ($custom_colors['plastictheme_comment_title_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= ".comment .title, #comments .title, .roundcorner-comment-title-bottom *, .roundcorner-comment-title-top *, .roundcorner-comment-top * { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_title_background'] ? $custom_colors['plastictheme_comment_title_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // The content area.
      if ($custom_colors['plastictheme_comment_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_comment_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "#comment, .comment, .comment .content, .comment .picture, .comment .submitted, .comment-page-content #comment-form, .preview .comment, .roundcorner-comment-bottom *, .roundcorner-comment-reply-bottom *, .roundcorner-comment-content { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_text'] ? $custom_colors['plastictheme_comment_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_background'] ? $custom_colors['plastictheme_comment_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "#comment, .comment, .comment .content, .comment .picture, .comment .submitted, .comment-page-content #comment-form, .preview .comment, .roundcorner-comment-bottom *, .roundcorner-comment-reply-bottom *, .roundcorner-comment-content { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_text'] ? $custom_colors['plastictheme_comment_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_comment_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "#comment, .comment, .comment .content, .comment .picture, .comment .submitted, .comment-page-content #comment-form, .preview .comment, .roundcorner-comment-bottom *, .roundcorner-comment-reply-bottom *, .roundcorner-comment-content { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_comment_background'] ? $custom_colors['plastictheme_comment_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";

        // I hate it when I do something different and don't comment why.
        $vars['plastictheme_comment_content_background']  = "<style type=\"text/css\">\n";
//        $vars['plastictheme_comment_content_background'] .= ".roundcorner-content-bottom *, .roundcorner-content-content, .roundcorner-welcome-bottom * { background-color: ";
        $vars['plastictheme_comment_content_background'] .= ".roundcorner-comment-bottom *, .roundcorner-comment-bottom { background-color: ";
        $vars['plastictheme_comment_content_background'] .= $custom_colors['plastictheme_comment_background'] ? $custom_colors['plastictheme_comment_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_comment_content_background'] .= "; }\n";
        $vars['plastictheme_comment_content_background'] .= "#content-area { background-color: ";
        $vars['plastictheme_comment_content_background'] .= $custom_colors['plastictheme_page_background'] ? $custom_colors['plastictheme_page_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_comment_content_background'] .= "; }\n";
        $vars['plastictheme_comment_content_background'] .= "</style>";
      }

      // Hyperlinks.
      // Unvisited.
      if ($custom_colors['plastictheme_link_color'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_link_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "a:link, li a.link { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_link_color'] ? $custom_colors['plastictheme_link_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_link_background'] ? $custom_colors['plastictheme_link_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "a:link, li a.link { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_link_color'] ? $custom_colors['plastictheme_link_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_link_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "a:link, li a.link { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_link_background'] ? $custom_colors['plastictheme_link_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // Visited.
      if ($custom_colors['plastictheme_visited_color'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_visited_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "a:visited, li a.visited { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_visited_color'] ? $custom_colors['plastictheme_visited_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_visited_background'] ? $custom_colors['plastictheme_visited_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "a:visited, li a.visited { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_visited_color'] ? $custom_colors['plastictheme_visited_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_visited_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "a:visited, li a.visited { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_visited_background'] ? $custom_colors['plastictheme_visited_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // Hover.
      if ($custom_colors['plastictheme_hover_color'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_hover_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "a:hover, li a.hover { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_hover_color'] ? $custom_colors['plastictheme_hover_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_hover_background'] ? $custom_colors['plastictheme_hover_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "a:hover, li a.hover { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_hover_color'] ? $custom_colors['plastictheme_hover_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_hover_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "a:hover, li a.hover { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_hover_background'] ? $custom_colors['plastictheme_hover_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // Focus.
      if ($custom_colors['plastictheme_focus_color'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_focus_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "a:focus, li a.focus { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_focus_color'] ? $custom_colors['plastictheme_focus_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_focus_background'] ? $custom_colors['plastictheme_focus_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "a:focus, li a.focus { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_focus_color'] ? $custom_colors['plastictheme_focus_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_focus_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "a:focus, li a.focus { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_focus_background'] ? $custom_colors['plastictheme_focus_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }
      // Active.
      if ($custom_colors['plastictheme_active_color'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_active_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "a:active, li a.active { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_active_color'] ? $custom_colors['plastictheme_active_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_active_background'] ? $custom_colors['plastictheme_active_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "a:active, li a.active { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_active_color'] ? $custom_colors['plastictheme_active_color'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_active_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "a:active, li a.active { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_active_background'] ? $custom_colors['plastictheme_active_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      // Admin areas.
      // The title text.
//      if ($custom_colors['plastictheme_admin_title'] || $custom_colors['plastictheme_default_color']) {
//        $vars['plastictheme_custom_colors'] .= ".block .title { color: ";
//        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_admin_title'] ? $custom_colors['plastictheme_admin_title'] : $custom_colors['plastictheme_default_color'];
//        $vars['plastictheme_custom_colors'] .= "; }\n";
//      }
      // The title background.
//      if ($custom_colors['plastictheme_admin_title_background'] || $custom_colors['plastictheme_default_color']) {
//        $vars['plastictheme_custom_colors'] .= ".block .title, .roundcorner-block-title-bottom *, .roundcorner-block-title-top * { background-color: ";
//        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_admin_title_background'] ? $custom_colors['plastictheme_admin_title_background'] : $custom_colors['plastictheme_default_color'];
//        $vars['plastictheme_custom_colors'] .= "; }\n";
//      }
      // The content area.
      if ($custom_colors['plastictheme_admin_content_text'] || $custom_colors['plastictheme_default_color']) {
        if ($custom_colors['plastictheme_admin_content_background'] || $custom_colors['plastictheme_default_color']) {
          $vars['plastictheme_custom_colors'] .= "admin, .admin-page-content, .admin-list, .admin-page-content .box, #block-admin-display, div.admin .left, div.admin .right, #book-admin-edit select, #book-admin-edit select.progress-disabled, #book-admin-edit tr.ahah-new-content, #book-admin-edit .form-item, .roundcorner-admin-bottom *, .roundcorner-admin-content, .roundcorner-admin-top * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_admin_content_text'] ? $custom_colors['plastictheme_admin_content_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; background-color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_admin_content_background'] ? $custom_colors['plastictheme_admin_content_background'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
        else {
          $vars['plastictheme_custom_colors'] .= "admin, .admin-page-content, .admin-list, .admin-page-content .box, #block-admin-display, div.admin .left, div.admin .right, #book-admin-edit select, #book-admin-edit select.progress-disabled, #book-admin-edit tr.ahah-new-content, #book-admin-edit .form-item, .roundcorner-admin-bottom *, .roundcorner-admin-content, .roundcorner-admin-top * { color: ";
          $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_admin_content_text'] ? $custom_colors['plastictheme_admin_content_text'] : $custom_colors['plastictheme_default_color'];
          $vars['plastictheme_custom_colors'] .= "; }\n";
        }
      }
      elseif ($custom_colors['plastictheme_admin_content_background'] || $custom_colors['plastictheme_default_color']) {
        $vars['plastictheme_custom_colors'] .= "admin, .admin-page-content, .admin-list, .admin-page-content .box, #block-admin-display, div.admin .left, div.admin .right, #book-admin-edit select, #book-admin-edit select.progress-disabled, #book-admin-edit tr.ahah-new-content, #book-admin-edit .form-item, .roundcorner-admin-bottom *, .roundcorner-admin-content, .roundcorner-admin-top * { background-color: ";
        $vars['plastictheme_custom_colors'] .= $custom_colors['plastictheme_admin_content_background'] ? $custom_colors['plastictheme_admin_content_background'] : $custom_colors['plastictheme_default_color'];
        $vars['plastictheme_custom_colors'] .= "; }\n";
      }

      $vars['plastictheme_custom_colors'] .= "</style>";

      break;
    }
  }


  // Calculate and set the height for the header of the page.
  $vars = _plastictheme_set_page_opener_height($vars, $theme_settings);

  // Specify the header height.
  if ($theme_settings['plastictheme_header_height']) {
    $vars['plastictheme_header_height']  = "<style type=\"text/css\">\n";
    $vars['plastictheme_header_height'] .= "#navigation { height: ";
    $vars['plastictheme_header_height'] .= $theme_settings['plastictheme_header_height'];
    $vars['plastictheme_header_height'] .= "; }\n";
    $vars['plastictheme_header_height'] .= ".with-navigation #content, .with-navigation .region-sidebar-first, .with-navigation .region-sidebar-second { margin-top: ";
    $vars['plastictheme_header_height'] .= $theme_settings['plastictheme_header_height'];
    $vars['plastictheme_header_height'] .= "; }\n";
    $vars['plastictheme_header_height'] .= "</style>";
  }
}


/**
 * Override or insert variables into the node templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 * /
function plastictheme_preprocess_node(&$vars, $hook) {

  // Optionally, run node-type-specific preprocess functions, like
  // plastictheme_preprocess_node_page() or plastictheme_preprocess_node_story().
//  $function = __FUNCTION__ . '_' . $vars['node']->type;
//  if (function_exists($function)) {
//    $function($vars, $hook);
//  }
}
//*/


/**
 * Override or insert variables into the comment templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 * /
function plastictheme_preprocess_comment(&$vars, $hook) {
}
//*/


/**
 * Override or insert variables into the block templates.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 * /
function plastictheme_preprocess_block(&$vars, $hook) {
}
//*/


/**
 * Calculate and set the height for the header of the page.
 *
 * The header height is the distance from the top of the page to the top of
 * the content area.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * /
function _plastictheme_set_header_height($vars, $theme_settings) {
dpm('1: $header_height = "' . $header_height . '"');
  // Specify some base height numbers, based on the css settings.
  // My assumption is that 1em=16px.
  // 2em=32px, 1.3em=21px, 3.3em=53px // 0.875em=14px, 1.286em=21 px, 2.161em=35px
  $site_name_height = 3.3;  //53; // 2em = 32px, 1.3em = 21px. 3.3em = 53px
  $site_slogan_height = 1.286;  //14;  // 0.875em = 14px
  $search_box_height = 1.286;  //21; // 1.286em = 21 px

  // If there is a logo, we need to find it and get the height.
  if ($vars['logo']) {
    $vars = _plastictheme_get_image_height($vars, $theme_settings);
/*
    $cwd = getcwd();
    $cwd_len = strlen($cwd);
    $cwd_last = substr($cwd, $cwd_len - 1, 1);
    $base_path = base_path();
    $base_path_len  = strlen($base_path);

    // If the base path is a single character, then it is "/".
    if ($base_path_len == 1) {
      // Check to see if the last character of $cwd is "/".
      if ($cwd_last != '/') {
        // $base_path is "/", $cwd does not end with a "/", and $vars['logo']
        // starts with a "/", therefore, we do not need $base_path and
        // $vars['logo'] can just be appended to $cwd.
        $logo_file_path = $cwd . $vars['logo'];
      }
      else {
        // $base_path is "/", $cwd also ends with a "/", and $vars['logo']
        // starts with a "/", therefore, we do not need $base_path but
        // we cannot just append $vars['logo'] to $cwd w/o first removing the
        // final "/" of $cwd.
        $logo_file_path = substr($cwd, 0, $cwd_len - 1) . $vars['logo'];
      }
    }
    // The base_path() funtion should always return at least "/". If it
    // is greater than one character, then it is likely to also be
    // appended to $cwd and prepended to $vars['logo']. We'll remove it from
    // $vars['logo'] and then append $vars['logo'] to $cwd.
    else {
      // Check to see if the last character of $cwd is "/".
      if ($cwd_last != '/') {
        // Append $vars['logo'] to $cwd, minus the part that is in $base_path,
        // adding a "/" between them.
        $logo_file_path = $cwd . "/" . substr($vars['logo'], $base_path_len, strlen($vars['logo']));
      }
      else {
        // Append $vars['logo'] to $cwd, minus the part that is in $base_path.
        $logo_file_path = $cwd . substr($vars['logo'], $base_path_len, strlen($vars['logo']));
      }
    }

    // Get the information about the logo, which includes its height.
    $logo_info = image_get_info($logo_file_path);

    // Free some memory by removing now unused variables.
    unset($cwd, $cwd_len, $cwd_last, $base_path, $base_path_len, $base_path_last, $logo_file_path);

    $header_option_height  = $vars['site_name'] ? $site_name_height : 0;
    $header_option_height += $vars['site_slogan'] ? $site_slogan_height : 0;
    $header_option_height += $vars['search_box'] ? $search_box_height : 0;

    $page_opener_height = ($logo_info['height'] / 16 + 1.5 > $header_option_height) ? $logo_info['height'] / 16 + 1.5 : $header_option_height;

    // Set the height of the page-opener block.
    $vars['plastictheme_page_opener']  = "<style type=\"text/css\">\n#page-opener { height: ";
    $vars['plastictheme_page_opener'] .= $page_opener_height;
    $vars['plastictheme_page_opener'] .= "em; }\n</style>";
//dpm('We have a logo: ' . $page_opener_height);
* /
  }

  // If the admin has specified the header height, use that.
  if ($theme_settings['plastictheme_header_height']) {
    $vars['plastictheme_header_height']  = "<style type=\"text/css\">\n";
    $vars['plastictheme_header_height'] .= "#navigation { height: ";
    $vars['plastictheme_header_height'] .= $theme_settings['plastictheme_header_height'];
    $vars['plastictheme_header_height'] .= "px; }\n";
    $vars['plastictheme_header_height'] .= ".with-navigation #content, .with-navigation .region-sidebar-first, .with-navigation .region-sidebar-second { margin-top: ";
    $vars['plastictheme_header_height'] .= $theme_settings['plastictheme_header_height'];
    $vars['plastictheme_header_height'] .= "px; }\n";
    $vars['plastictheme_header_height'] .= "</style>";
//dpm('The admin has specified a height: ' . $theme_settings['plastictheme_header_height']);
  }
  // Guess the height.
  else {
    $body_margin = $header_height = 0.75;  //12;
dpm('2: $header_height = "' . $header_height . '"');
    $rounded_corner_height = 0.375;  //6;
    $title_height = 1.375;  //22;
    $text_padding_height = 0.5;  //8;
    $text_height = 0.875;  //14;
    $bottom_margin_height = 0.75;  //12;

    // Add whatever was enabled of either the logo, or the site name, site
    // slogan, and search box.
//    if ($vars['logo'] || $vars['site_name'] || $vars['site_slogan'] || $vars['search_box']) {
//      if ($page_opener_height > $header_option_height) {
//        $header_height += $page_opener_height;
//dpm('3: $header_height = "' . $header_height . '"');
//dpm('The page_opener_height is greater than the header_option_height: ' . $page_opener_height);
//      }
//      else {
//        $header_height += $vars['site_name'] ? $site_name_height : 0;
//dpm('4: $header_height = "' . $header_height . '"');
//        $header_height += $vars['site_slogan'] ? $site_slogan_height : 0;
//dpm('5: $header_height = "' . $header_height . '"');
//        $header_height += $vars['search_box'] ? $search_box_height : 0;
//dpm('6: $header_height = "' . $header_height . '"');
//dpm('The header_option_height is greater than the page_opener_height: ' . $header_option_height);
//      }

    if ($vars['logo'] || $vars['site_name'] || $vars['site_slogan'] || $vars['search_box']) {
      // Add the height for the rounded corners, if they were enabled.
      $header_height += ($theme_settings['plastictheme_enable_upper_left'] || $theme_settings['plastictheme_enable_upper_right']) ? $rounded_corner_height : 0;
dpm('7: $header_height = "' . $header_height . '"');
      $header_height += ($theme_settings['plastictheme_enable_lower_left'] || $theme_settings['plastictheme_enable_lower_right']) ? $rounded_corner_height : 0;
dpm('8: $header_height = "' . $header_height . '"');

      // If anything was specified for the page opener, add the height for the
      // margin-bottom spacer.
      $header_height += ($header_height > $body_margin) ? $bottom_margin_height : 0;
dpm('9: $header_height = "' . $header_height . '"');
    }

    // Add the height for the content header, if it was enabled.
    if ($vars['header']) {
dpm('Header:');
      $header = $title_height + $text_padding_height + $text_height + $text_padding_height + $bottom_margin_height;
      $header_height += $header;
dpm('10: $header_height = "' . $header_height . '"');

      // Add the height for the rounded corners, if they were enabled.
      $header_height += ($theme_settings['plastictheme_enable_upper_left'] || $theme_settings['plastictheme_enable_upper_right']) ? $rounded_corner_height : 0;
dpm('11: $header_height = "' . $header_height . '"');
      $header_height += ($theme_settings['plastictheme_enable_lower_left'] || $theme_settings['plastictheme_enable_lower_right']) ? $rounded_corner_height : 0;
dpm('12: $header_height = "' . $header_height . '"');
//dpm('We have a header: ' . $header);
    }

    // Add the height for the primary or secondary links, if either are
    // enabled.
     if ($vars['primary_links'] || $vars['secondary_links']) {
dpm('Primary links:');
      $primary_links = $text_padding_height + $text_height + $text_padding_height + $bottom_margin_height;
      $header_height += $primary_links;
dpm('13: $header_height = "' . $header_height . '"');

      // Add the height for the rounded corners, if they were enabled.
      $header_height += ($theme_settings['plastictheme_enable_upper_left'] || $theme_settings['plastictheme_enable_upper_right']) ? $rounded_corner_height : 0;
dpm('14: $header_height = "' . $header_height . '"');
      $header_height += ($theme_settings['plastictheme_enable_lower_left'] || $theme_settings['plastictheme_enable_lower_right']) ? $rounded_corner_height : 0;
dpm('15: $header_height = "' . $header_height . '"');
//dpm('We have primary links: ' . $primary_links);
    }

    // Add the height for the navigation, if it was enabled.
    if ($vars['navigation']) {
dpm('Navigation:');
      $navigation = $title_height + $text_padding_height + $text_height + $text_padding_height + $bottom_margin_height + 2.5;
//      $navigation = $title_height + $text_padding_height + $text_height + $text_padding_height;
      $header_height += $navigation;
dpm('16: $header_height = "' . $header_height . '"');

      // Add the height for the rounded corners, if they were enabled.
      $header_height += ($theme_settings['plastictheme_enable_upper_left'] || $theme_settings['plastictheme_enable_upper_right']) ? $rounded_corner_height : 0;
dpm('17: $header_height = "' . $header_height . '"');
      $header_height += ($theme_settings['plastictheme_enable_lower_left'] || $theme_settings['plastictheme_enable_lower_right']) ? $rounded_corner_height : 0;
dpm('18: $header_height = "' . $header_height . '"');
//dpm('We have navigation: ' . $navigation);
    }

    // Create the relevant CSS code.
    if ($header_height) {
      $vars['plastictheme_header_height']  = "<style type=\"text/css\">\n";
      $vars['plastictheme_header_height'] .= "#navigation { height: ";
      $vars['plastictheme_header_height'] .= ($header_height) / 2;
      $vars['plastictheme_header_height'] .= "em; }\n";
      $vars['plastictheme_header_height'] .= ".with-navigation #content, .with-navigation .region-sidebar-first, .with-navigation .region-sidebar-second { margin-top: ";
      $vars['plastictheme_header_height'] .= ($header_height) / 2;
      $vars['plastictheme_header_height'] .= "em; }\n";
      $vars['plastictheme_header_height'] .= "</style>";
//dpm('We are creating the CSS code.');
    }
//dpm('We are guessing at the height: ' . $header_height);
  }

dpm('$header_height = "' . $header_height . '"; $header_height*16 = "' . $header_height * 16 . '"');
  return $vars;
}
//*/


/**
 * Calculate and set the height for the header of the page.
 *
 * The header height is the distance from the top of the page to the top of
 * the content area.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 * @param $theme_settings
 *   An array of variables defined by the system administrator.
 *
 * @return
 *   An array of variables to return to the theme template.
 */
function _plastictheme_set_page_opener_height($vars, $theme_settings) {
  // Specify some base height numbers, based on the css settings.
  // My assumption is that 1em=16px.
  // 2em=32px, 1.3em=21px, 3.3em=53px // 0.875em=14px, 1.286em=21 px, 2.161em=35px
  $site_name_height = 53;  //53; // 2em = 32px, 1.3em = 21px. 3.3em = 53px
  $site_slogan_height = 14;  //14;  // 0.875em = 14px
  $search_box_height = 21;  //21; // 1.286em = 21 px

  // Get the information about the logo, which includes its height.
  $logo_height = _plastictheme_get_image_height($vars);

  $header_option_height  = $vars['site_name'] ? $site_name_height : 0;
  $header_option_height += $vars['site_slogan'] ? $site_slogan_height : 0;
  $header_option_height += $vars['search_box'] ? $search_box_height : 0;

//  $page_opener_height = ($logo_height / 16 + 1.5 > $header_option_height) ? $logo_height / 16 + 1.5 : $header_option_height;
  $page_opener_height = ($logo_height > $header_option_height) ? $logo_height : $header_option_height;

  // Set the height of the page-opener block.
  $vars['plastictheme_page_opener']  = "<style type=\"text/css\">\n#page-opener { height: ";
  $vars['plastictheme_page_opener'] .= $page_opener_height;
//  $vars['plastictheme_page_opener'] .= "em; }\n</style>";
  $vars['plastictheme_page_opener'] .= "px; }\n</style>";

  return $vars;
}
//*/


/**
 * Calculate and return the height for the logo.
 *
 * @param $vars
 *   An array of variables to pass to the theme template.
 *
 * @return
 *   An integer containing the height of the logo.
 */
function _plastictheme_get_image_height($vars) {
  $cwd = getcwd();
  $cwd_len = strlen($cwd);
  $cwd_last = substr($cwd, $cwd_len - 1, 1);
  $base_path = base_path();
  $base_path_len  = strlen($base_path);

  // If the base path is a single character, then it is "/".
  if ($base_path_len == 1) {
    // Check to see if the last character of $cwd is "/".
    if ($cwd_last != '/') {
      // $base_path is "/", $cwd does not end with a "/", and $vars['logo']
      // starts with a "/", therefore, we do not need $base_path and
      // $vars['logo'] can just be appended to $cwd.
      $logo_file_path = $cwd . $vars['logo'];
    }
    else {
      // $base_path is "/", $cwd also ends with a "/", and $vars['logo']
      // starts with a "/", therefore, we do not need $base_path but
      // we cannot just append $vars['logo'] to $cwd w/o first removing the
      // final "/" of $cwd.
      $logo_file_path = substr($cwd, 0, $cwd_len - 1) . $vars['logo'];
    }
  }
  // The base_path() funtion should always return at least "/". If it
  // is greater than one character, then it is likely to also be
  // appended to $cwd and prepended to $vars['logo']. We'll remove it from
  // $vars['logo'] and then append $vars['logo'] to $cwd.
  else {
    // Check to see if the last character of $cwd is "/".
    if ($cwd_last != '/') {
      // Append $vars['logo'] to $cwd, minus the part that is in $base_path,
      // adding a "/" between them.
      $logo_file_path = $cwd . "/" . substr($vars['logo'], $base_path_len, strlen($vars['logo']));
    }
    else {
      // Append $vars['logo'] to $cwd, minus the part that is in $base_path.
      $logo_file_path = $cwd . substr($vars['logo'], $base_path_len, strlen($vars['logo']));
    }
  }

  // Get the information about the logo, which includes its height.
  $logo_info = image_get_info($logo_file_path);

  // Return the logo's height.
  return $logo_info['height'];
}
//*/
